/*
 * Copyright (c) 2016-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.repository.viewer.github;

import org.marvelution.bamboo.repository.viewer.CommitUriProvider;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class GitHubUriProviderTest {

	private static final String COMMIT_ID = "feda65bbd252c5f28bd47fa33d5898e6a78f5882";
	private static final String FILE_NAME = "pom.xml";

	private CommitUriProvider commitUriProvider;

	@Before
	public void setUp() throws Exception {
		commitUriProvider = new GitHubUriProvider("marvelution", "bamboo-repository-viewers", "master");
	}

	@Test
	public void testGetUrlForFile() throws Exception {
		assertThat(commitUriProvider.getUrlForFile(COMMIT_ID, FILE_NAME),
		           is("https://github.com/marvelution/bamboo-repository-viewers/blob/" + COMMIT_ID + "/" + FILE_NAME));
	}

	@Test
	public void testGetUrlForFile_NoRevision() throws Exception {
		assertThat(commitUriProvider.getUrlForFile(null, FILE_NAME),
		           is("https://github.com/marvelution/bamboo-repository-viewers/blob/master/" + FILE_NAME));
	}

	@Test
	public void testGetUrlForFile_NoFile() throws Exception {
		assertThat(commitUriProvider.getUrlForFile(COMMIT_ID, null),
		           is("https://github.com/marvelution/bamboo-repository-viewers/commit/" + COMMIT_ID));
	}

	@Test
	public void testGetUrlForChangeset() throws Exception {
		assertThat(commitUriProvider.getUrlForChangeset(COMMIT_ID),
		           is("https://github.com/marvelution/bamboo-repository-viewers/commit/" + COMMIT_ID));
	}

	@Test
	public void testGetUrlForChangeset_NoFile() throws Exception {
		assertThat(commitUriProvider.getUrlForChangeset(COMMIT_ID, null, 0),
		           is("https://github.com/marvelution/bamboo-repository-viewers/commit/" + COMMIT_ID));
	}

	@Test
	public void testGetUrlForChangeset_ForFile() throws Exception {
		assertThat(commitUriProvider.getUrlForChangeset(COMMIT_ID, FILE_NAME, 1),
		           is("https://github.com/marvelution/bamboo-repository-viewers/commit/" + COMMIT_ID + "#diff-1"));
	}

}
