/*
 * Copyright (c) 2016-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.repository.viewer;

import org.marvelution.bamboo.repository.viewer.bitbucket.BitbucketUriProvider;
import org.marvelution.bamboo.repository.viewer.github.GitHubUriProvider;

import com.atlassian.bamboo.plan.branch.VcsBranchImpl;
import com.atlassian.bamboo.plugins.bitbucket.BitbucketRepository;
import com.atlassian.bamboo.plugins.git.GitHubRepository;
import com.atlassian.bamboo.plugins.git.GitRepository;
import com.atlassian.bamboo.repository.RepositoryData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class CommitUriProviderTest {

	private static final String OWNER = "marvelution";
	private static final String REPOSITORY_NAME = "bamboo-repository-viewers";

	private static final String BB_URL = "https://bitbucket.org/" + OWNER + "/" + REPOSITORY_NAME;
	private static final String BB_HTTPS = BB_URL + ".git";
	private static final String BB_GIT = "git://bitbucket.org/" + OWNER + "/" + REPOSITORY_NAME + ".git";
	private static final String BB_SSH = "git@bitbucket.org:" + OWNER + "/" + REPOSITORY_NAME + ".git";

	private static final String GH_URL = "https://github.com/" + OWNER + "/" + REPOSITORY_NAME;
	private static final String GH_HTTPS = GH_URL + ".git";
	private static final String GH_GIT = "git://github.com/" + OWNER + "/" + REPOSITORY_NAME + ".git";
	private static final String GH_SSH = "git@github.com:" + OWNER + "/" + REPOSITORY_NAME + ".git";

	@Mock
	private RepositoryData repositoryData;

	@Test
	public void testGetCommitUriProvider_BitbucketRepository() throws Exception {
		BitbucketRepository bitbucketRepository = mock(BitbucketRepository.class);
		GitRepository repository = mock(GitRepository.class);
		when(bitbucketRepository.getGitRepository()).thenReturn(repository);
		when(repository.getRepositoryUrl()).thenReturn(BB_HTTPS);
		when(repository.getVcsBranch()).thenReturn(new VcsBranchImpl("master"));
		when(repositoryData.getRepository()).thenReturn(bitbucketRepository);
		assertCommitUriProvider(BitbucketUriProvider.class, BB_URL);
	}

	@Test
	public void testGetCommitUriProvider_GithubRepository() throws Exception {
		GitHubRepository gitHubRepository = mock(GitHubRepository.class);
		GitRepository repository = mock(GitRepository.class);
		when(gitHubRepository.getGitRepository()).thenReturn(repository);
		when(repository.getRepositoryUrl()).thenReturn(GH_HTTPS);
		when(repository.getVcsBranch()).thenReturn(new VcsBranchImpl("master"));
		when(repositoryData.getRepository()).thenReturn(gitHubRepository);
		assertCommitUriProvider(GitHubUriProvider.class, GH_URL);
	}

	@Test
	public void testGetCommitUriProvider_GitRepository_HTTPS_bitbucket() throws Exception {
		testGetCommitUriProvider_GitRepository(BB_HTTPS, BitbucketUriProvider.class, BB_URL);
	}

	@Test
	public void testGetCommitUriProvider_GitRepository_GIT_bitbucket() throws Exception {
		testGetCommitUriProvider_GitRepository(BB_GIT, BitbucketUriProvider.class, BB_URL);
	}

	@Test
	public void testGetCommitUriProvider_GitRepository_SSH_bitbucket() throws Exception {
		testGetCommitUriProvider_GitRepository(BB_SSH, BitbucketUriProvider.class, BB_URL);
	}

	@Test
	public void testGetCommitUriProvider_GitRepository_HTTPS_github() throws Exception {
		testGetCommitUriProvider_GitRepository(GH_HTTPS, GitHubUriProvider.class, GH_URL);
	}

	@Test
	public void testGetCommitUriProvider_GitRepository_GIT_github() throws Exception {
		testGetCommitUriProvider_GitRepository(GH_GIT, GitHubUriProvider.class, GH_URL);
	}

	@Test
	public void testGetCommitUriProvider_GitRepository_SSH_github() throws Exception {
		testGetCommitUriProvider_GitRepository(GH_SSH, GitHubUriProvider.class, GH_URL);
	}

	private void testGetCommitUriProvider_GitRepository(String repositoryUrl, Class<? extends CommitUriProvider> expectedType,
	                                                    String expectedUrl) {
		GitRepository repository = mock(GitRepository.class);
		when(repository.getRepositoryUrl()).thenReturn(repositoryUrl);
		when(repository.getVcsBranch()).thenReturn(new VcsBranchImpl("master"));
		when(repositoryData.getRepository()).thenReturn(repository);
		assertCommitUriProvider(expectedType, expectedUrl);
	}

	private void assertCommitUriProvider(Class<? extends CommitUriProvider> expectedType, String exceptedUrl) {
		CommitUriProvider provider = CommitUriProvider.getCommitUriProvider(repositoryData);
		assertThat(provider, instanceOf(expectedType));
		assertThat(provider.getRepositoryUrl(), is(exceptedUrl));
	}

}
