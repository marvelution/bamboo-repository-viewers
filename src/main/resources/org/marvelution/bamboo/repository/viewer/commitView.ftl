[#--
  ~ Copyright (c) 2016-present Marvelution B.V.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~    http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  --]
[#-- @ftlvariable name="linkGenerator" type="org.marvelution.bamboo.repository.viewer.GitWebRepositoryViewer" --]
[#-- @ftlvariable name="commit" type="com.atlassian.bamboo.commit.Commit" --]
[#-- @ftlvariable name="repositoryData" type="com.atlassian.bamboo.repository.RepositoryData" --]
[#-- @ftlvariable name="buildResultsSummary" type="com.atlassian.bamboo.resultsummary.BuildResultsSummary" --]
[#-- @ftlvariable name="repositoryChangeset" type="com.atlassian.bamboo.resultsummary.vcs.RepositoryChangeset" --]

[#-- This template is a customized version of the one provide by Bamboo in 'templates/plugins/webRepository/defaultCommitView.ftl' --]
[#if repositoryChangeset.commits?has_content]
	[@ui.bambooInfoDisplay title=repositoryChangeset.repositoryData.name?html headerWeight='h2']
	<ul>
		[#list repositoryChangeset.commits.toArray()?sort_by("date")?reverse as commit]
			<li>
				[@ui.displayAuthorAvatarForCommit commit=commit avatarSize='24' /]
				<h3>
					<a href="[@cp.displayAuthorOrProfileLink author=commit.author /]">[@ui.displayAuthorFullName author=commit.author /]</a>
					<span class="revision-date">[@ui.time datetime=commit.date relative=true /]</span>
					[#if "Unknown" != commit.author.name]
						[#assign commitUrl = (linkGenerator.getWebRepositoryUrlForCommit(commit, repositoryData))!('') /]
						[#assign guessedRevision = commit.guessChangeSetId()!("")]
						[#if commitUrl?has_content && guessedRevision?has_content]
							<a href="${commitUrl}" class="revision-id"
							   title="[@ww.text name="webRepositoryViewer.viewChangeset" /]">${guessedRevision}</a>
						[#else ]
							<span class="revision-id"
							      title="[@ww.text name='webRepositoryViewer.error.cantCreateUrl' /]">${commit.changeSetId!}</span>
						[/#if]
						[#if commit.foreignCommit!false]
							[@ui.lozenge textKey="commit.merged"  showTitle=false/]
						[/#if]
					[/#if]
				</h3>
				<p>[@ui.renderValidJiraIssues commit.comment buildResultsSummary /]</p>
				<ul class="files">
					[#list commit.files as file]
						<li>
							[#if "Unknown" != commit.author.name]
								[#assign fileLink = linkGenerator.getWebRepositoryUrlForFile(file, repositoryData)!]
								[#if fileLink?has_content]
									<a href="${fileLink}">${file.cleanName}</a>
								[#else]
								${file.name}
								[/#if]
								[#if file.revision?has_content]
									[#assign revisionLink = linkGenerator.getWebRepositoryUrlForRevision(file, repositoryData)!]
									[#if fileLink?has_content && revisionLink?has_content]
										<a href="${revisionLink}">(version ${file.revision})</a>
									[#else]
										(version ${file.revision})
									[/#if]
									[#assign diffLink = linkGenerator.getWebRepositoryUrlForDiff(file, file_index, repositoryData)!]
									[#if fileLink?has_content && diffLink?has_content]
										<a href="${diffLink}">(diffs)</a>
									[/#if]
								[/#if]
							[#else]
							${file.cleanName!}
								[#if file.revision??]
									(version ${file.revision})
								[/#if]
							[/#if]
						</li>
					[/#list]
				</ul>
			</li>
		[/#list]
	</ul>
	[/@ui.bambooInfoDisplay]
[/#if]
