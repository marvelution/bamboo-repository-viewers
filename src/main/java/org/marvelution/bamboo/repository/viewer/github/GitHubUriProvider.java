/*
 * Copyright (c) 2016-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.repository.viewer.github;

import javax.annotation.Nonnull;

import org.marvelution.bamboo.repository.viewer.CommitUriProvider;

import static java.lang.String.format;

/**
 * {@link CommitUriProvider} implementation to provide URIs for revisions and files in the GitHub Web UI
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class GitHubUriProvider extends CommitUriProvider {

	public GitHubUriProvider(@Nonnull String owner, @Nonnull String repository, String branch) {
		super(format("https://github.com/%s/%s", owner, repository), branch);
	}

	@Override
	protected String getUrlForCommit(@Nonnull String revision) {
		return format("%s/commit/%s", repositoryBaseUrl, revision);
	}

	@Override
	protected String getUrlForBlob(@Nonnull String revision, @Nonnull String file) {
		return format("%s/blob/%s/%s", repositoryBaseUrl, revision, file);
	}

	@Override
	protected String getUrlForDiff(@Nonnull String revision, @Nonnull String file, int index) {
		return format("%s#diff-%s", getUrlForCommit(revision), index);
	}

}
