/*
 * Copyright (c) 2016-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.repository.viewer;

import java.net.URI;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNullableByDefault;

import org.marvelution.bamboo.repository.viewer.bitbucket.BitbucketUriProvider;
import org.marvelution.bamboo.repository.viewer.github.GitHubUriProvider;

import com.atlassian.bamboo.plugins.git.GitRepository;
import com.atlassian.bamboo.plugins.git.GitRepositoryFacade;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.RepositoryData;

import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.apache.commons.lang3.StringUtils.removeStart;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Base implementation of the Commit URI provider used by the {@link GitWebRepositoryViewer}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ParametersAreNullableByDefault
public abstract class CommitUriProvider {

	private static final CommitUriProvider NULL_COMMIT_URI_PROVIDER = new NullCommitUriProvider();
	protected final String repositoryBaseUrl;
	protected final String branch;

	protected CommitUriProvider(@Nonnull String repositoryBaseUrl, String branch) {
		this.repositoryBaseUrl = repositoryBaseUrl;
		this.branch = ofNullable(branch).orElse("master");
	}

	/**
	 * Creates a new {@link CommitUriProvider} for the specified {@link RepositoryData repositoryData}.
	 * If the {@link Repository} of the specified {@link RepositoryData repositoryData} is not supported then a {@literal null} returning
	 * implementation is returned.
	 */
	@Nonnull
	static CommitUriProvider getCommitUriProvider(@Nonnull RepositoryData repositoryData) {
		GitRepository repository = null;
		if (repositoryData.getRepository() instanceof GitRepository) {
			repository = (GitRepository) repositoryData.getRepository();
		} else if (repositoryData.getRepository() instanceof GitRepositoryFacade) {
			repository = ((GitRepositoryFacade) repositoryData.getRepository()).getGitRepository();
		}
		if (repository != null) {
			String[] parts = null;
			try {
				URI repositoryUrl = URI.create(repository.getRepositoryUrl());
				parts = new String[] { repositoryUrl.getHost(), removeEnd(removeStart(repositoryUrl.getPath(), "/"), ".git") };
			} catch (IllegalArgumentException e) {
				// the repositoryUrl is not a well formed URI, check if its a SSH URL instead
				if (repository.getRepositoryUrl().startsWith("git@")) {
					String cleanUrl = removeEnd(removeStart(repository.getRepositoryUrl(), "git@"), ".git");
					parts = cleanUrl.split(":", 2);
				}
			}
			if (parts != null) {
				String[] ownerRepo = parts[1].split("/", 2);
				switch (parts[0]) {
					case "github.com":
						return new GitHubUriProvider(ownerRepo[0], ownerRepo[1], repository.getVcsBranch().getName());
					case "bitbucket.org":
						return new BitbucketUriProvider(ownerRepo[0], ownerRepo[1], repository.getVcsBranch().getName());
				}
			}
		}
		getLogger(CommitUriProvider.class).warn("Unable to get CommitUriProvider for repository '{}'", repositoryData.getName());
		return NULL_COMMIT_URI_PROVIDER;
	}

	/**
	 * Returns the base web url for the repository
	 */
	@Nonnull
	public String getRepositoryUrl() {
		return repositoryBaseUrl;
	}

	/**
	 * Returns the url to for the specified {@literal file} of the specified {@literal revision}, or {@literal null}.
	 * <ol>
	 * <li>If the {@literal file} is {@literal null}, then the url to the revision is returned.</li>
	 * <li>If the {@literal revision} is {@literal null}, then the repository {@link #branch branch name} is used.</li>
	 * </ol>
	 */
	@Nullable
	public String getUrlForFile(String revision, String file) {
		return (isNull(file)) ? getUrlForCommit(revisionOrBranch(revision)) : getUrlForBlob(revisionOrBranch(revision), file);
	}

	/**
	 * Returns the url to for the specified {@literal revision}, or {@literal null}.
	 * <ol>
	 * <li>If the {@literal revision} is {@literal null}, then the repository {@link #branch branch name} is used.</li>
	 * </ol>
	 */
	@Nullable
	public String getUrlForChangeset(String revision) {
		return getUrlForCommit(revisionOrBranch(revision));
	}

	/**
	 * Returns the url to for the specified {@literal file} within the specified {@literal revision}, or {@literal null}.
	 * <ol>
	 * <li>If the {@literal file} is {@literal null}, then the url to the revision is returned.</li>
	 * <li>If the {@literal revision} is {@literal null}, then the repository {@link #branch branch name} is used.</li>
	 * </ol>
	 */
	@Nullable
	public String getUrlForChangeset(String revision, String file, int index) {
		return isNull(file) ? getUrlForCommit(revisionOrBranch(revision)) : getUrlForDiff(revisionOrBranch(revision), file, index);
	}

	/**
	 * Returns the commit url for the specified {@literal revision}, may be {@literal null}.
	 */
	@Nullable
	protected abstract String getUrlForCommit(@Nonnull String revision);

	/**
	 * Returns the file url for the specified {@literal file} of the specified {@literal revision}, may be {@literal null}.
	 */
	@Nullable
	protected abstract String getUrlForBlob(@Nonnull String revision, @Nonnull String file);

	/**
	 * Returns the commit url for the specified {@literal file} within the specified {@literal revision}, may be {@literal null}.
	 */
	@Nullable
	protected abstract String getUrlForDiff(@Nonnull String revision, @Nonnull String file, int index);

	/**
	 * Returns either the specified {@literal revision} if its not {@literal null}, otherwise it will return the
	 * {@link #branch branch name}.
	 */
	@Nonnull
	protected String revisionOrBranch(String revision) {
		return ofNullable(revision).orElse(branch);
	}

	/**
	 * {@literal null} returning {@link CommitUriProvider} implementation
	 */
	private static class NullCommitUriProvider extends CommitUriProvider {

		NullCommitUriProvider() {
			super("http://unknown-host/unknown-owner/unknown-repository", null);
		}

		@Override
		protected String getUrlForCommit(String revision) {
			return null;
		}

		@Override
		protected String getUrlForBlob(String revision, @Nonnull String file) {
			return null;
		}

		@Override
		protected String getUrlForDiff(String revision, @Nonnull String file, int index) {
			return null;
		}

	}

}
