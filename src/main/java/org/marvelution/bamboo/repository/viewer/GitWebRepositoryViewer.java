/*
 * Copyright (c) 2016-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.repository.viewer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import javax.annotation.Nonnull;

import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.commit.CommitFile;
import com.atlassian.bamboo.deployments.versions.history.commit.DeploymentVersionVcsCommit;
import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.vcs.RepositoryChangeset;
import com.atlassian.bamboo.webrepository.AbstractWebRepositoryViewer;
import com.atlassian.bamboo.webrepository.DeploymentsAwareCommitUrlProvider;
import com.atlassian.bamboo.webrepository.WebRepositoryViewer;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toMap;

/**
 * {@link WebRepositoryViewer} implementation that uses the {@link CommitUriProvider} to generate urls of commits.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class GitWebRepositoryViewer extends AbstractWebRepositoryViewer implements DeploymentsAwareCommitUrlProvider {

	private static final String FULL_COMMIT_VIEW_TEMPLATE = "org/marvelution/bamboo/repository/viewer/commitView.ftl";
	private static final String SUMMARY_COMMIT_VIEW_TEMPLATE = "templates/plugins/webRepository/commonCommitSummaryView.ftl";

	@Nonnull
	@Override
	public Collection<String> getSupportedRepositories() {
		return asList(
				"com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-bitbucket:bb",
				"com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:gh",
				"com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:git",
				"com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:gitv2"
		);
	}

	@Override
	public String getHtmlForCommitsFull(@Nonnull ResultsSummary resultsSummary, @Nonnull RepositoryChangeset repositoryChangeset,
	                                    @Nonnull RepositoryData repositoryData) {
		Map<String, Object> context = new HashMap<>();
		context.put("buildResultsSummary", resultsSummary);
		context.put("repositoryChangeset", repositoryChangeset);
		context.put("repositoryData", repositoryData);
		context.put("linkGenerator", this);
		return templateRenderer.render(FULL_COMMIT_VIEW_TEMPLATE, context);
	}

	@Override
	public String getHtmlForCommitsSummary(@Nonnull ResultsSummary resultsSummary, @Nonnull RepositoryChangeset repositoryChangeset,
	                                       @Nonnull RepositoryData repositoryData, int maxChanges) {
		Map<String, Object> context = new HashMap<>();
		context.put("buildResultsSummary", resultsSummary);
		context.put("repositoryChangeset", repositoryChangeset);
		context.put("repositoryData", repositoryData);
		context.put("linkGenerator", this);
		context.put("maxChanges", maxChanges);
		return templateRenderer.render(SUMMARY_COMMIT_VIEW_TEMPLATE, context);
	}

	@Override
	public String getWebRepositoryUrlForCommit(Commit commit, @Nonnull RepositoryData repositoryData) {
		return CommitUriProvider.getCommitUriProvider(repositoryData).getUrlForChangeset(commit.guessChangeSetId());
	}

	@Override
	public String getWebRepositoryUrlForRevision(String revisionId, @Nonnull RepositoryData repositoryDefinition) {
		return CommitUriProvider.getCommitUriProvider(repositoryDefinition).getUrlForChangeset(revisionId);
	}

	@Override
	public Map<Commit, String> getWebRepositoryUrlForCommits(Collection<Commit> commits, RepositoryData repositoryData) {
		CommitUriProvider uriProvider = CommitUriProvider.getCommitUriProvider(repositoryData);
		return commits.stream().collect(toMap(Function.identity(), commit ->
				uriProvider.getUrlForChangeset(commit.guessChangeSetId())
		));
	}

	@Override
	public Map<DeploymentVersionVcsCommit, String> getWebRepositoryUrlForDeploymentVersionCommits(
			Collection<DeploymentVersionVcsCommit> commits, RepositoryData repositoryData) {
		CommitUriProvider uriProvider = CommitUriProvider.getCommitUriProvider(repositoryData);
		return commits.stream().collect(toMap(Function.identity(), commit ->
				uriProvider.getUrlForChangeset(commit.getChangeSetId())
		));
	}

	public String getWebRepositoryUrlForFile(CommitFile file, RepositoryData repositoryData) {
		return CommitUriProvider.getCommitUriProvider(repositoryData).getUrlForFile(null, file.getName());
	}

	public String getWebRepositoryUrlForRevision(CommitFile file, RepositoryData repositoryData) {
		return CommitUriProvider.getCommitUriProvider(repositoryData).getUrlForFile(file.getRevision(), file.getName());
	}

	public String getWebRepositoryUrlForDiff(CommitFile file, int index, RepositoryData repositoryData) {
		return CommitUriProvider.getCommitUriProvider(repositoryData).getUrlForChangeset(file.getRevision(), file.getName(), index);
	}

}
