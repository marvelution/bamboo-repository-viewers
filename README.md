This projects features a custom Git [WebRepositoryViewer](https://developer.atlassian.com/bamboodev/bamboo-plugin-guide/bamboo-plugin-module-types/user-interface-plugin-modules/web-repository-viewer-module)
for [Atlassian Bamboo](https://www.atlassian.com/software/bamboo).

Continuous Builder
==================
<https://builds.marvelution.org/browse/MC-BRV>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
